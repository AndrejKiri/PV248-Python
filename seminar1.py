#Part 1: Text & Regular Expressions

import re
import collections

score = {}

with open('scorelib.txt', 'r') as library:
    for line in library:
        # String operations
        if ':' in line:
            clear_line = line.strip()
            info = clear_line.split(':')
            dic = info[0]
            item = info[1]

            # Dictionaries
            if dic not in score:
                score[dic] = {}
            if item not in score[dic]:
                score[dic][item] = 0
            score[dic][item] += 1

# Tasks: How many pieces by each composer?
print('How many pieces by each composer?')
for k, v in score['Composer'].items():
    print(k, ":", v)

print('\n')

# Tasks: How many pieces composed in a given century?
score['Composition Year 2'] = {}
score['Composition Century'] = {}
for k, v in score['Composition Year'].items():
    for s in k.split():
         if s.isdigit():
             if s not in score['Composition Year 2']:
                 score['Composition Year 2'][s] = 0
             score['Composition Year 2'][s] += v*1

for k, v in score['Composition Year 2'].items():
        if k[:2] not in score['Composition Century']:
            score['Composition Century'][k[:2]] = 0
        score['Composition Century'][k[:2]] += v*1

SortedCentury = collections.OrderedDict(sorted(score['Composition Century'].items()))

print('How many pieces composed in a given century?')
for k, v in SortedCentury.items():
    print(k, "th century:", v)

print('\n')

# Tasks: How many in the key of c minor?
print('How many in the key of c minor?')
print(score['Key'][' c minor'])

print('\n')
