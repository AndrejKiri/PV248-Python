#Part 9: Testing and Debugging


'''
suppose an 𝑛-dimensional space
• 𝑛 + 1 points determine an 𝑛-simplex
− 3 points determine a 2D triangle
− 4 points determine a 3D tetrahedron
• pick one of the points
− subtract it from the others to get 𝑛 vectors
− put the vectors into columns of an 𝑛 × 𝑛 matrix
• volume of an 𝑛-parallelotope is the determinant
− divide by 𝑛! to get 𝑛-simplex volume

P1
• start with def volume(*args): pass
• write unit tests for this volume function
• check with a few obvious examples
− it’s enough to check in 2D and 3D
− do at least 4–5 distinct cases
• also check behaviour for invalid inputs
− mismatched number of points vs dimension
− extraneous components in points

P2
• actually implement the volume function
− take variable number of parameters
− for def f(*args), args is a tuple
• compute 𝑛-simplex volume
− only makes sense for 3 or more points
− use numpy to do the math
• check that the unit tests pass
'''

from unittest import TestCase
from math import factorial, sqrt, fabs
import pdb
import numpy as np

def volume(*args):
    # Make sure all arguments are lists and have propper number of dimensions
    points = len(args)
    for point in args:
        if type(point) != list:
            raise Exception('One or more points do not have propper type.')
        dimensions = len(point)
        if points != (dimensions + 1):
            raise Exception('One or more points do not have propper number of dimensions.')

    # Number of dimensions
    dimensions = points - 1

    # Vectors calculation - [[1,0][2,0]]
    flag = True
    matrix = []
    for point in args:
        if flag:
            pointX = point
            flag = False
        else:
            vector = []
            for i in range(len(args)-1):
                x = pointX[i] - point[i]
                vector.append(x)
            matrix.append(vector)
    array = np.array(matrix)
    determinant = np.linalg.det(array)
    parallelotopeVolume = fabs(round(determinant, 2))

    # Final result calculation and return
    result = parallelotopeVolume / factorial(dimensions)
    return result

a = [6,6,6]
b = [0,0,0]
c = [0,6,0]
d = [6,6,0]
triangle = volume(a, b, c, d)
print(triangle)

class TestVolume(TestCase):
    def test_example_2D_1(self):
        a = [2,2]
        b = [0,0]
        c = [0,2]
        triangle = volume(a, b, c)
        self.assertEqual(triangle, 2)

    def test_example_2D_2(self):
        a = [4,4]
        b = [0,0]
        c = [0,4]
        triangle = volume(a, b, c)
        self.assertEqual(triangle, 8)

    def test_example_2D_3(self):
        a = [10,10]
        b = [0,0]
        c = [0,10]
        triangle = volume(a, b, c)
        self.assertEqual(triangle, 50)

    def test_example_3D_1(self):
        a = [6,6,6]
        b = [0,0,0]
        c = [0,0,6]
        d = [0,6,6]
        tetrahedron = volume(a, b, c, d)
        self.assertEqual(tetrahedron, 36)

    def test_example_3D_2(self):
        a = [6,6,6]
        b = [0,0,0]
        c = [0,0,6]
        d = [6,0,6]
        tetrahedron = volume(a, b, c, d)
        self.assertEqual(tetrahedron, 36)

    def test_points_to_dimensions(self):
        with self.assertRaises(Exception) as context:
            a = [10,10,10,10]
            b = [0,0,0]
            c = [0,0,10]
            d = [10,0,10]
            tetrahedron = volume(a, b, c, d)
            self.assertTrue('One or more points do not have propper number of dimensions.' in context.exception)

    def test_point_type(self):
        with self.assertRaises(Exception) as context:
            a = 2
            b = [0,0,0]
            c = [0,0,10]
            d = [10,0,10]
            tetrahedron = volume(a, b, c, d)
            self.assertTrue('One or more points do not have propper type.' in context.exception)
