#Part 3: SQL Redux & JSON

import sqlite3
import json
import sys

conn = sqlite3.connect( "scorelib.dat" )
cur = conn.cursor()
cur.execute('SELECT Person.name, Score.key FROM Person CROSS JOIN Score_author CROSS JOIN Score WHERE Score_author.score = Score.id AND Score_author.composer = Person.id')
data = cur.fetchall()

# data: (1, 1685, 1750, 'Bach, Johann Sebastian')
d = []

for line in data:
    d.append({'Name' : line[0], 'Key' : line[1]})

json.dump( d, sys.stdout, indent=4 )

print(sys.argv[1])


#json.dump( d, sys.stdout, indent=4 )
