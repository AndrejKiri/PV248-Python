from labyrint import *
from flask import Flask, request, jsonify
import json
#empty url - GET = OK, POST = Path
#not empty url - GET, POST = 404
#HEAD = not defined
#other methods = 405

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        result = shortestPath(data)
        return result
    elif request.method == 'GET':
        return ''

app.run()
