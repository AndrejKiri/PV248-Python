import networkx as nx
import json
from unittest import TestCase
#python3 -m unittest labyrint.py

#The code
def shortestPath(input):
    #load json as graph
    try:
        G = nx.Graph()
        nodes = []
        flag_same_names = False
        flag_not_first_item = False
        for node in input["rooms"]:
            G.add_node(node)
            if flag_not_first_item:
                for item in nodes:
                    if item == node:
                        flag_same_names = True
            else:
                flag_not_first_item = True
            nodes.append(node)
        if flag_same_names == True:
            raise KeyError
        for edge in input["corridors"]:
            G.add_edge(edge[0], edge[1])
        #compute shortest path
        path = nx.shortest_path(G, source = input["start"], target = input["end"])
        #return the path + required stuff
        result = {"solution": None,
         "length": None,
         "status": None
         }
        result["solution"]=path
        result["length"]=len(path)
        result["status"]="OK"
        outputdata = json.dumps(result, ensure_ascii=False)
        return outputdata
    except nx.NetworkXNoPath:
        result = {"solution": None,
         "length": None,
         "status": None
         }
        result["status"]="No solution found"
        outputdata = json.dumps(result, ensure_ascii=False)
        return outputdata
    except (KeyError, nx.NodeNotFound):
        result = {"solution": None,
         "length": None,
         "error": None
         }
        result["error"]="Invalid input"
        outputdata = json.dumps(result, ensure_ascii=False)
        return outputdata
'''
#Dev
data = {'rooms': ['A', 'B'], 'corridors': [], 'start': 'A', 'end': 'X'}
json_dat = json.loads(data.replace("'",'"'))
result = shortestPath(json_dat)
print(result)
'''
#Tests
class TestShortestPath(TestCase):
    def test_valid_result(self):
        test_data = """ {
        "start": "A",
        "end": "E",
        "rooms": ["A","B","C","D","E","F"],
        "corridors": [ ["A","B"], ["B","E"], ["B","C"], ["C","E"]] }
        """
        test_json = json.loads(test_data)
        x = shortestPath(test_json)
        self.assertEqual(x,'{"solution": ["A", "B", "E"], "length": 3, "status": "OK"}')
    def test_invalid_noresult(self):
        test_data = """ {
        "start": "A",
        "end": "F",
        "rooms": ["A","B","C","D","E","F"],
        "corridors": [ ["A","B"], ["B","E"], ["B","C"], ["C","E"]] }
        """
        test_json = json.loads(test_data)
        x = shortestPath(test_json)
        self.assertEqual(x,'{"solution": null, "length": null, "status": "No solution found"}')
    def test_invalid_invalidinput(self):
        test_data = """ {
        "starter": "A",
        "end": "E",
        "rooms": ["A","B","C","D","E","F"],
        "corridors": [ ["A","B"], ["B","E"], ["B","C"], ["C","E"] , ["E","F"]] }
        """
        test_json = json.loads(test_data)
        x = shortestPath(test_json)
        self.assertEqual(x,'{"solution": null, "length": null, "status": "Invalid input"}')
