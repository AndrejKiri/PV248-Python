import re
import numpy as np

readable1 = '2 x + 3 y = 5'
readable2 = 'x - y = 0'

print(readable1)
print(readable2)

equations = [readable1, readable2]

right = []
left = {}

for equation in equations:
    #No whitespace
    r1 = re.compile( r"(\S)" )
    step1 = ''.join(r1.findall(equation))
    #Right side
    r2 = re.compile( r"(.*)=(\d)" )
    step2 = r2.findall(step1)
    value = int(step2[0][1])
    right.append(value)
    #All items from left side
    r3 = re.compile( r"([\+|\-]*\d*[a-z])" )
    step3 = r3.findall(step2[0][0])
    #Split item, check value and assign to dict
    for item in step3:
        r4 = re.compile( r"([\+|\-]*)(\d*)([a-z])" )
        step4 = r4.findall(item)
        key = step4[0][2]
        value = step4[0][1]
        #Value should be number
        if value == '':
            value = 1
        else:
            value = int(value)
        #Minus or plus
        if step4[0][0] == '-':
            value = value * (-1)
        #Save
        if key in left:
            left[key].append(value)
        else:
            left[key] = []
            left[key].append(value)
#Dict to list
x = len(left[key])
y = len(left.keys())
leftlist = []
while y > 0:
    leftlist.append([])
    y -= 1
for i in range(x):
    for key in left:
        leftlist[i].append(left[key][i])
#List to array
part1 = np.array(leftlist)
part2 = np.array(right)

#The solution
y = len(left.keys())
name = list(left.keys())
solution = np.linalg.solve(part1, part2)
print('Solution is:')
while y > 0:
    y -= 1
    print(name[y],' = ', solution[y])
