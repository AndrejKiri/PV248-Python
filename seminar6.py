#Part 5 - Serving HTTP


'''
implement a simple HTTP server
− listen on a high port (e.g. 8000)
− point your browser to http://localhost:8000/
• part 1: serve some static text (or HTML)
• part 2: get & print back some data from the URL
− e.g. when serving http://localhost:8000/file.txt
− return “you asked for file.txt”
'''
from http.server import HTTPServer, BaseHTTPRequestHandler

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        pathString = self.path
        if len(pathString) == 1:
            message = "<h1>Hello world!</h1>"
        else:
            message = "<h1>You asked for " + pathString + "!!</h1>"
        self.wfile.write(bytes(message, "utf8"))
        return

def run(server_class=HTTPServer, handler_class=Handler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    print("Its alive!!!")
    httpd.serve_forever()

run()
