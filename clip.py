from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.io import show, output_file
from operator import itemgetter
from numpy import pi
import json

#Load JSON
with open('election.json') as datafile:
    data = json.load(datafile)
#Sort data
data2 = sorted(data, key=itemgetter('votes'), reverse=True)
#Exclude parties that got less than 1%
other = {'name' : 'Other', 'votes' : 0}
data3 = []
for party in data2:
    if party['share']<=1.0:
        other['votes'] += party['votes']
    else:
        data3.append(party)
data3.append(other)
#List of pary names
labels = []
#List of votes
scores = []
#List of colors
colors = []
#Legend
substituted = ''
#Fill lists
for party in data3:
    try:
        labels.append(party['short'])
        substituted+=(party['short'] + ' - ' + party['name'] + '|')
    except KeyError:
        if len(party['name'])<15:
            labels.append(party['name'])
        else:
            part = party['name'][0:15]
            labels.append(part+'...')
    try:
        colors.append(party['color'])
    except KeyError:
        colors.append('#595d60')
    scores.append(party['votes'])

#Specify size of a piece of the pie
pie = [0]
start = 0
summary = sum(scores)
for s in scores:
    percent = s / summary
    cumulative = start + percent
    pie.append(cumulative)
    start = cumulative
starts = [p*2*pi for p in pie[:-1]]
ends = [p*2*pi for p in pie[1:]]

#Create Column Data Source
src = ColumnDataSource( data = {
    'labels': labels,
    'scores': scores,
    'starts': starts,
    'ends': ends,
    'colors': colors} )

#Pie chart
output_file("pie2.html")
p = figure(plot_width=1200, plot_height=700, title="Election 2017: Results", x_range=(-1,1), y_range=(-1,1))
p.wedge( x = 0, y = 0, radius = 0.5,
    start_angle = 'starts',
    end_angle = 'ends',
    color = 'colors',
    legend = 'labels',
    source = src )
show(p)

#Bar chart
output_file("simple_bar_plot2.html")
r = figure( plot_width=1200, plot_height=700, x_range = labels, title="Election 2017: Results")
r.xaxis.major_label_orientation = 1
r.vbar( x = 'labels',
    top = 'scores',
    width = 0.7,
    fill_color='colors',
    legend = 'labels',
    source=src)
show(r)
