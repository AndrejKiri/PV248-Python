# -


# Weather API: Find current weather for Brno airport in Aviation weather
import requests
import xml.etree.ElementTree as etree

response = requests.get('https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=LKTB&hoursBeforeNow=2')
#print(response.content)

tree = etree.fromstring(response.content)
for element in tree.findall('.//data/METAR'):
    temperature = element.find('./temp_c').text
    print(temperature)
    break
