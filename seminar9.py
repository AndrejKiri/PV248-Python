#Part 8: Animations with Pygame


from pygame import display, draw, time, event, key
from random import randint
import pygame

'''
Exercise 8: Raindrops / Bubbles
• do a screen-saver-like animation at 60 fps
• draw expanding circles on the screen
− put them in random locations
− expansion at 1 pixel per frame works nicely
• remove circles when they get too big
• add new circles as old ones disappear
− 1 new circle per frame works
• quit when the user hits a key
'''
pygame.init()

screen = display.set_mode([800, 600])
screen.fill([0, 0, 0])

class Bubble:
    def __init__(self):
        self.colour = [200, 100, 100]
        x = randint(1 , 800)
        y = randint(1 , 600)
        self.center = [x , y]
        self.radius = 1

    def expand(self):
        self.radius += 1

    def replace(self):
        self.radius = 1
        x = randint(1 , 800)
        y = randint(1 , 600)
        self.center = [x , y]

bubbles = []

def createBubbles():
    if len(bubbles) < 50 and randint(1, 100) < 20:
        instance = Bubble()
        bubbles.append(instance)

def drawBubbles():
    for bubble in bubbles:
        draw.circle(screen, bubble.colour, bubble.center, bubble.radius, 1)
        if bubble.radius < 100:
            bubble.expand()
        else:
            bubble.replace()

while True:
    screen.fill([0, 0, 0])
    createBubbles()
    drawBubbles()
    display.flip()
    event = pygame.event.poll()
    if event.type == pygame.KEYDOWN:
        break
    time.wait(20)
